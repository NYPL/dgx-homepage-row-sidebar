// Import libraires
import React from 'react';

// Import components
import Sidebar from './components/Sidebar/Sidebar.jsx';


/* app.jsx
 * Used for local development of React Components
 */

// This is the section of dummy conten only for test
let dummyContent = {
  // title:'Of Note',
  // link: 'www.nypl.org'
},
  dummyErrorType = 'error!';

React.render(<Sidebar content={dummyContent} errorType={dummyErrorType} />, document.body);
