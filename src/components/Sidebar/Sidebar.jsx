// Import libraires
import React from 'react';
import _ from 'underscore';

// Import components
import { SeeMoreButton } from 'dgx-react-buttons';

class Sidebar extends React.Component {

  constructor(props) {
    super(props);

    this.state = {}
  }

  componentDidMount() {}

  componentWillUnmount() {}

  render() {
    let content = (this.props.content) ? this.props.content : {},
      title = content.title,
      link = content.link,
      errorType = (this.props.errorType) ? this.props.errorType : '';

      console.log(content.length);

    // If any error from the API server
    if (errorType.length) {
      return (
        <div>
           Here is an {errorType} error.
        </div>
      );
    } else {
      // Then check if the content is empty
      if (!content || _.isEmpty(content)) {
        return (
          <div>
             Here is no content.
          </div>
        );
      } else {
        return (
          <div>
            <div>
              {title}
            </div>
            <SeeMoreButton />
          </div>
        );
      }
    }
  }
};

Sidebar.defaultProps = {
  id: 'Sidebar',
  className: 'Sidebar',
  content: {},
  lang: 'en'
};

export default Sidebar;
